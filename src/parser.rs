pub struct Parser {
    pub original_arguments: Vec<String>
}

pub enum ProgramOptions<'a> {
    Hit(&'a str, Vec<String>), // String is target (must be URL),
              // Vec<String> is the rest of the arguments
    HitMissingArgument,
    Help(&'a str), // String is command whose usage we want
    HelpMissingArgument,
    NoArgumentsGiven,
    UnknownOption(&'a str),
}

impl Parser {
    pub fn parse_args(&self) -> ProgramOptions{
        match self.original_arguments.len() {
            1 => ProgramOptions::NoArgumentsGiven,
            _ => match self.original_arguments[1].as_str() {
                "help" => match self.original_arguments.len() {
                    2 => ProgramOptions::HelpMissingArgument,
                    _ => ProgramOptions::Help(&self.original_arguments[2])
                },
                "hit" => match self.original_arguments.len() {
                    2 => ProgramOptions::HitMissingArgument,
                    _ => ProgramOptions::Hit(&self.original_arguments[2], self.original_arguments[3..].to_vec()),
                },
                _ => ProgramOptions::UnknownOption(self.original_arguments[1].as_str()),
            }
        }
    }
    pub fn new(arguments: Vec<String>) -> Parser{
        Parser{
           original_arguments: arguments,
        }
    }
}