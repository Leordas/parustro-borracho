mod parser;
use std::env;

fn main() {
    let arguments: Vec<String> = env::args().collect();
    let parser = parser::Parser::new(arguments);
    let output = match parser.parse_args() {
        parser::ProgramOptions::UnknownOption(option) => format!("unknown option '{}'", option),
        parser::ProgramOptions::NoArgumentsGiven => String::from("no arguments given"),
        parser::ProgramOptions::Hit(target, options) => format!("should hit {} with options {:?}", target, options),
        parser::ProgramOptions::HitMissingArgument => String::from("missing hit argument"),
        parser::ProgramOptions::Help(argument) => format!("should do help on option {}", argument),
        parser::ProgramOptions::HelpMissingArgument => String::from("missing help argument")
    };
    println!("{}",output);
}
